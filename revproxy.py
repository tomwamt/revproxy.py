import http.server
import http.client
import socket


def find(itbl, pred):
	for e in itbl:
		if pred(e):
			return e
	return None


class HTTPReverseProxyServer(http.server.HTTPServer):
	def __init__(self, address, mappings):
		super().__init__(address, HTTPReverseProxyHandler)
		self.proxy_mappings = dict(mappings)


class HTTPReverseProxyHandler(http.server.BaseHTTPRequestHandler):
	def handle_one_request(self):
		try:
			self.raw_requestline = self.rfile.readline(65537)
			if len(self.raw_requestline) > 65536:
				self.requestline = ''
				self.request_version = ''
				self.command = ''
				self.send_error(HTTPStatus.REQUEST_URI_TOO_LONG)
				return
			if not self.raw_requestline:
				self.close_connection = True
				return
			if not self.parse_request():
				# An error code has been sent, just exit
				return

			lpath = find(self.server.proxy_mappings, lambda p: self.path.startswith(p))
			if lpath is None:
				self.send_error(HTTPStatus.NOT_FOUND)
				return

			host, r_basepath = self.server.proxy_mappings[lpath]
			rpath = r_basepath.rstrip('/') + '/' + self.path[len(lpath):].lstrip('/')

			self.fwd_req(host, rpath)
			
			self.wfile.flush() #actually send the response if not already done.
		except socket.timeout as e:
			#a read or a write timed out.  Discard this connection
			self.log_error("Request timed out: %r", e)
			self.close_connection = True
			return

	def fwd_req(self, host, path):
		conn = http.client.HTTPConnection(host)
		reqbody = ''
		if self.headers['Content-Length'] is not None:
			reqbody = self.rfile.read(int(self.headers['Content-Length']))
		conn.request(self.command, path, reqbody, self.headers)

		resp = conn.getresponse()
		self.send_response_only(resp.status, resp.reason)
		for key, value in resp.getheaders():
			self.send_header(key, value)
		self.end_headers()
		self.wfile.write(resp.read())
		conn.close()


def main(args):
	mappings = {}

	if args.config is not None:
		import json
		with open(args.config) as f:
			mappings = json.load(f)

	for mstr in args.mappings:
		key, val = mstr.split('=', maxsplit=1)
		host, path = val.split('/', maxsplit=1) if '/' in val else (val, '')
		mappings[key] = host, '/' + path

	# validate and echo structure of mappings
	for key, (host, path) in mappings.items():
		print(f'{key} => {host}{path}')

	try:
		with HTTPReverseProxyServer(('', args.port), mappings) as server:
			print(f'Server started on port {args.port}')
			server.serve_forever()
	except KeyboardInterrupt:
		print()


if __name__ == '__main__':
	from argparse import ArgumentParser

	parser = ArgumentParser(description='Simple HTTP reverse proxy server')
	parser.add_argument('-p', '--port', type=int, default=8080, help='Default port 8080')
	parser.add_argument('-m', '--mapping', action='append', default=[], dest='mappings', help='Mapping in the form of "/some/path=hostname:1234/foo/bar"')
	parser.add_argument('config', nargs='?', default=None, help='JSON mapping config')

	main(parser.parse_args())
