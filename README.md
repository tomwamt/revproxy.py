# revproxy.py

Simple HTTP reverse proxy server

usage: revproxy.py \[-h\] \[-p PORT\] \[-m MAPPINGS\] \[config\]

### Example config.json

```json
{
	"/api": ["localhost:8082", "/api"],
	"/": ["localhost:8081", ""]
}
```

Order of mappings matter!

### Example cmd line

```bash
python revproxy.py config.json -m /images=www.example.com/images
```
